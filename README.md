# squad-data

Tabular datasets built from `squad` using `squad-client`

## Re-populating

It takes about a week to run this from scratch with an empty lkft directory. To
speed up that process, an existing large request_cache DB can be used.

A large existing cache can be found at https://storage.lkft.org/squad-data/.
Download and extract `qa-reports.sqlite.xz` (it's about 70GB extracted) before
running `build.py`.

## Re-publishing

In the event that all csvs need to be changed (like adding a column), it's best to remove `lkft/` from git's history and then repopulate it.

To remove `lkft/` from git history, run:

```
git filter-branch --tree-filter 'rm -rf lkft' --prune-empty HEAD
```

After that, a force push will be necessary.

