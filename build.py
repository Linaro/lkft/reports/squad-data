import os
import requests_cache
import pandas as pd
from squad_client.core.api import SquadApi
from squad_client.core.models import Squad, ALL

requests_cache.install_cache("qa-reports")

root_path = "."
projects = [
    # group, project
    ("lkft", "linux-next-master"),
    ("lkft", "linux-mainline-master"),
    ("lkft", "linux-stable-rc-linux-5.9.y"),
    ("lkft", "linux-stable-rc-linux-5.8.y"),
    ("lkft", "linux-stable-rc-linux-5.7.y"),
    ("lkft", "linux-stable-rc-linux-5.6.y"),
    ("lkft", "linux-stable-rc-linux-5.5.y"),
    ("lkft", "linux-stable-rc-linux-5.4.y"),
    ("lkft", "linux-stable-rc-linux-5.3.y"),
    ("lkft", "linux-stable-rc-linux-5.2.y"),
    ("lkft", "linux-stable-rc-linux-5.1.y"),
    ("lkft", "linux-stable-rc-linux-5.0.y"),
    ("lkft", "linux-stable-rc-linux-4.20.y"),
    ("lkft", "linux-stable-rc-linux-4.19.y"),
    ("lkft", "linux-stable-rc-linux-4.18.y"),
    ("lkft", "linux-stable-rc-linux-4.17.y"),
    ("lkft", "linux-stable-rc-linux-4.16.y"),
    ("lkft", "linux-stable-rc-linux-4.15.y"),
    ("lkft", "linux-stable-rc-linux-4.14.y"),
    ("lkft", "linux-stable-rc-linux-4.13.y"),
    ("lkft", "linux-stable-rc-linux-4.9.y"),
    ("lkft", "linux-stable-rc-linux-4.4.y"),
]

SquadApi.configure(url="https://qa-reports.linaro.org/")


class SquadDataSet(object):
    def __init__(self, group, project, build):
        self.group = group
        self.project = project
        self.build = build
        self.dataset = []

    def add_test(self, environment, test):
        self.dataset.append(
            {
                "group.name": self.group.slug,
                "project.name": self.project.slug,
                "build.name": self.build.version,
                "environment.name": environment.slug,
                "test.name": test.name,
                "test.status": test.status,
            }
        )

for _group, _project in projects:
    group = Squad().group(_group)
    project = group.project(_project)
    for build in project.builds(count=ALL).values():
        if not build.finished:
            print(f"Skipping unfinished build {build.version}")
            continue

        csv_path = os.path.join(
            root_path, group.slug, project.slug, build.version + ".csv"
        )

        if os.path.exists(csv_path):
            print(f"Skipping {csv_path}...")
            continue

        print(f"Building {csv_path}...")
        sds = SquadDataSet(group, project, build)

        for testrun in build.testruns().values():
            environment = testrun.environment
            for test in testrun.tests().values():
                sds.add_test(environment, test)

        os.makedirs(os.path.dirname(csv_path), exist_ok=True)
        df = pd.DataFrame(data=sds.dataset)
        # df.sort_values(by=['First Column','Second Column',...], inplace=True)
        df.to_csv(csv_path, encoding="utf-8", index=False)
